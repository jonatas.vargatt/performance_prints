# Automatizar prints de performance

Script para gerar múltiplos relatórios de performance através do site [https://pagespeed.web.dev/](https://pagespeed.web.dev/).

## Rodar o projeto:
- Clone o projeto e instale as dependências com o comando `"npm i"`.
- No array `urls`, certifique-se de inserir as URLs e nomes das páginas que deseja obter a performance.

```javascript
const urls = [
  { url: 'https://auaonline2.vnda.dev/', page: 'home' },
  { url: 'https://auaonline2.vnda.dev/p/sobre', page: 'sobre' },
  { url: 'https://auaonline2.vnda.dev/produtos', page: 'tag' },
  { url: 'https://auaonline2.vnda.dev/produto/nome-do-produto-lorem-ipsum-2', page: 'produto' }
];
```

- Execute o script com o comando `"npm run start"`.