const puppeteer = require('puppeteer');

(async () => {
  const browser = await puppeteer.launch({
    headless: 'new'
  });
  const page = await browser.newPage();

  const urlInputSelector = 'input[name="url"]';
  const insightsXPath = '/html/body/c-wiz/div[2]/div/div[2]/div[3]/div/div/div[2]/span/div/div[2]/div[2]/div/div/article/div/div[2]/div/div/div/div[2]';
  const desktopXPath = '/html/body/c-wiz/div[2]/div/div[2]/div[3]/div/div/div[1]';

  await page.goto('https://pagespeed.web.dev');
  await page.waitForSelector(urlInputSelector);

  async function insights(link) {
    await page.focus(urlInputSelector);
    await page.keyboard.type(link);
    await page.keyboard.press('Enter');
    try {
      await page.waitForXPath(insightsXPath, { visible: true });
    }catch (error) {
      console.log('Página não encontrada.');
    }
    await new Promise(resolve => setTimeout(resolve, 3000));

  }

  async function captureScreenshot(name, desktop = false) {
    if (desktop) {
      await page.evaluate(() => {
        window.scrollTo(0, 0);
      });
      await page.click('#desktop_tab');
      await page.waitForXPath(desktopXPath, { visible: true });
      await new Promise(resolve => setTimeout(resolve, 3000));
    }

    await page.setViewport({ width: 1280, height: 1600 });
    await page.evaluate(() => {
      document.getElementById('kO001e').style.visibility = 'hidden';
      document.querySelector('.glAfi').style.visibility = 'hidden';
    });
    await new Promise(resolve => setTimeout(resolve, 3000));
    const screenshotPath = `./screenshots/${name}.png`;
    await page.screenshot({ path: screenshotPath });
  }

  async function clear() {
    const urlInput = await page.$(urlInputSelector);
    await urlInput.click({ clickCount: 3 });
    await page.keyboard.press('Backspace');
  }

  const urls = [
    { url: 'https://auaonline2.vnda.dev/', page: 'home' },
    { url: 'https://auaonline2.vnda.dev/p/sobre', page: 'sobre' },
    { url: 'https://auaonline2.vnda.dev/produtos', page: 'tag' },
    { url: 'https://auaonline2.vnda.dev/produto/nome-do-produto-lorem-ipsum-2', page: 'produto' }
  ];

  for (const { url, page: pageName } of urls) {
    const siteName = url.match(/\/\/(.*?)\.(vnda\.dev|com\.br|com)/)[1];

    console.log("Gerando print de:", url)
    await insights(url);
    await captureScreenshot(`${siteName}-mobile-${pageName}`);
    await captureScreenshot(`${siteName}-desktop-${pageName}`, true);
    await clear();
  }

  await browser.close();
})();
